// by using higher order functions,find the cube of each element in an array
//list the elements which are odd
//product of elements of an array
//list the array elements which are more than given length

//cube of elements using map
let highernumber =[2,4,6,8];
let cube =highernumber.map((number)=>number*number*number)
console.log(cube);

//filter to list odd elements
let num =[16,17,23,25,22,42,49];
let oddnumbers = num.filter((num)=>num%2!==0)
console.log(oddnumbers);

//reduce 
let product=[2,5,7,9]
let currentvalue =product.reduce((acc,currentvalue)=>acc*currentvalue)
console.log(currentvalue);

//array elements which are more than given length
let array1 = [ 'goutham','bhavya','bhavs','monica'];
let result3=array1.filter (name=>name.length>5);
console.log(result3);