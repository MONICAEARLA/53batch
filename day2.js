// Data Types
// Variables
// Functions
// For loop
// Each loop
// Math
// Random


console.log("FLOP")
const skills =["java","python","html","css"]
//for (initialization;condition;incr/decr)
//for loop

for (let i=0;i<skills.length;i++){
    console.log(skills[i]);
}
//for each loop
//statement
skills.forEach((element)=>{
     console.log(element);
})

//functions --reusable block of code
//function need declaration and calling
//function should return a value
//default function
//parameterized function
//parameters and arguments


function greet(name1,name2){
    return"GOOD MORNING "  +name1+ " "+name2;
}

let  name1 ="MOUNIKA"
let  name2 ="GOUTAM"
let greeting =greet(name1,name2);
console.log(greeting)

function addition(num1,num2){
    return num1+num2;
}
let addedval =addition(3,5);
console.log(addedval)

//write a function to give square,cube of number

function square(a){
    return a*a;
}
var value=square(15);
console.log(value);

function cube(b){
    return b*b*b;
}
 var val=cube(15);
 console.log(val);

 //write a function to find the max num in the given array
 
 //math
console.log(Math.max(3,6)); //6
console.log(Math.ceil(59.98)); //60
console.log(Math.ceil(59.98)); //60
console.log(Math.floor(55.98)); //55
console.log(Math.abs (-567)); //567
console.log(Math.round(Math.sqrt(2)));
console.log(Math.floor(Math.random()*100)+1)
console.log(Math.pow(2,10));

//write a function whether a number is even or odd number
  let i = 1;
  for (i=1;i<= 100;i++){
    if(i%2==0){
    console.log(i + "is even")
    }
}
let j = 1;
for (j=1;j<= 100;j++){
    if(j%2!==0){
        console.log(j+ "is add")
    }
}


//find the max element in the array
function findmax(arr){
    let maxnum=0;
    arr.forEach((element)=>{
        if(element>maxnum){ 
            maxnum =element;
        }
    })
    return maxnum;
}
let listelements =[10,12,5,89,90,100]
console.log(findmax(listelements));

//finding max element of an array by using math.max mathod
let arr =[10,30,40,50,100]
console.log(Math.max(...arr));

//what is diff bt array function and normal function
//array objects
let person ={
    name1 : 'MOUNIKA',
    Batch:53,
    present :true,
    skills:['java','html']
}
console.log(`name of the person is ${person.name1} and she is batch 53`);


 











